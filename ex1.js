function fakeAjax(url,cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);

	setTimeout(function(){
		cb(fake_responses[url]);
	},randomDelay);
}
function getFile(file) {
	return new Promise((resolve,reject) =>{
		fakeAjax(file,function(text){
			if(text===undefined){
				reject("file not found");
			}else{
				resolve(text);
			}
			// what do we do here?
		});
	})
}
async function getAllFiles(...files){
	try{
		let text1 = await getFile(files[0]);
		let text2 = await getFile(files[1]);
		let text3 = await getFile(files[2]);
		let text4 = await getFile(files[3]);
		console.log(text1);
		console.log(text2);
		console.log(text3);
		console.log(text4);
	}catch(err){
		console.error(err);
	}
}
getAllFiles("file1","file2","file3","file4");